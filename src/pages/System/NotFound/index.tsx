import { ErrorOutlined } from '@mui/icons-material';
import React from 'react';

function NotFound() {
  return (
    <div>
        <ErrorOutlined fontSize='large'/>
    </div>
  )
};

export default NotFound;