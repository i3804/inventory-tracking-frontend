import { Add } from '@mui/icons-material';
import { Button, Typography } from '@mui/material';
import React from 'react'
import DeviceTypeCard from '../../components/DeviceTypeCard';
import DeviceTypeModal from '../../components/DeviceTypeModal';
import { useDeviceTypesStore } from '../../store/DeviceTypesStore/provider';
import './styles/index.scss';

const DeviceTypesPage: React.FC = (): JSX.Element => {
	const deviceTypesStore = useDeviceTypesStore();
	const [modalOpen, setModalOpen] = React.useState<boolean>(false);

	const handleOpenCloseModal = (open: boolean) => (): void => {
		open ? setModalOpen(true) : setModalOpen(false);
	};

	return (
		<div className='device-types-page'>
			{deviceTypesStore?.deviceTypes?.map((deviceType) => <DeviceTypeCard key={deviceType.id} data={deviceType}/>)}
			<Button onClick={handleOpenCloseModal(true)}>
				<Add/>
				<Typography>
					Добавить тип девайса
				</Typography>
			</Button>
			<DeviceTypeModal open={modalOpen} handleClose={handleOpenCloseModal(false)}/>
		</div>
	);
};

export default DeviceTypesPage;