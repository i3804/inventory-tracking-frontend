import { Tab, Tabs } from '@mui/material';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Tag } from '../../api/models/Tag';
import TagList from '../../components/TagList';
import { useTagStore } from '../../store/TagStore/provider';
import './styles/index.scss';

const TagPage: React.FC = observer((): JSX.Element => {

	const tagStore = useTagStore();
	const [chosenTab, setChosenTab] = React.useState<number>(0);
	const [tagListData, setTagListData] = React.useState<Array<Tag>>(tagStore.tags);

	React.useEffect(() => {
		setTagListData(tagStore.tags);
	}, [tagStore.tags])

	const handleTabChange = (_: React.SyntheticEvent, value: number): void => {
		setChosenTab(value);
		setTagListData((prevState) => {
			if (chosenTab === value) return prevState;
			switch (value) {
				case 0: return tagStore.tags;
				case 1: return tagStore.presenseTags;
				case 2: return tagStore.absenseTags;
				case 3: return tagStore.inactiveTags;
				default: return tagStore.tags;
			}
		});
	}

    return (
		<div className='tag-page-wrapper'>
			<Tabs className='tags-tabs-panel' value={chosenTab} onChange={handleTabChange}>
				<Tab label="Все метки"/>
				<Tab disabled={!tagStore.presenseTags.length} label="Присутствующие"/>
				<Tab disabled={!tagStore.absenseTags.length} label="Отсутствующие"/>
				<Tab disabled={!tagStore.inactiveTags.length} label="Неактивные"/>
			</Tabs>

			<TagList data={tagListData} value={chosenTab}/>
		</div>
    );
});

export default TagPage;