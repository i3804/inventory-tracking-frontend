import { Typography } from '@mui/material';
import { observer } from 'mobx-react';
import React from 'react';
import { Tag } from '../../api/models/Tag';
// import { useTagStore } from '../../store/TagStore/provider';
import TagCard from '../TagCard';
import './styles/index.scss';

interface TagListProps {
    value: number;
    data: Array<Tag>;
}

const TagList: React.FC<TagListProps> = observer((props): JSX.Element => {
    const {value, data} = props;

    const renderTagList = (): Array<JSX.Element | null> | null => {
        return data.map(tag => {
            switch (value) {
                case 0: {
                    let className = !tag.active ? 'inactive' : tag.absence ? 'absent' : 'present';
                    return <TagCard className={`tag-card-${className}`} key={tag.hash} data={tag} />;
                };
                case 1: return !tag.absence ? <TagCard className='tag-card-present' key={tag.hash} data={tag} /> : null;
                case 2: return tag.absence ? <TagCard className='tag-card-absent' key={tag.hash} data={tag} /> : null;
                case 3: return !tag.active ? <TagCard className='tag-card-inactive' key={tag.hash} data={tag} /> : null;
                default: return <TagCard key={tag.hash} data={tag} />;
            }
        });
    };

    return (
        <div className='tag-list-wrapper'>
            {data.length ? renderTagList() : <Typography>Nothing's here...</Typography>}  
        </div>
    );
});

export default TagList;