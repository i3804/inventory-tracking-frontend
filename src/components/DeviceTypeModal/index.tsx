import { Button, Input, InputBaseProps, Modal, TextField } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import { api } from '../../api/services/http';

interface DeviceTypeModalProps {
    open: boolean;
    handleClose: () => void;
}

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'white',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

const DeviceTypeModal: React.FC<DeviceTypeModalProps> = (props: DeviceTypeModalProps): JSX.Element => {
    const {open, handleClose} = props; 
    const [deviceName, setDeviceName] = React.useState<string>("");
    const [errors, setErrors] = React.useState<Record<string, string>>({});

    const handleAddingDeviceType = (): void => {
        if (!handleValidateField()) return;
        api.createDeviceType({name: deviceName, picture: new Blob()});
        handleClose();
    }

    const handleValidateField = (): boolean => {
        if (deviceName.startsWith('`')) {
            setErrors(prevState => {
                return {
                    ...prevState,
                    deviceName: 'Некорректное имя!'
                }
            });
            return false;
        }
        return true;
    }

    return (
        <Modal open={open} onClose={handleClose}>
            <Box sx={style}>
                <h3>Добавление типа девайса</h3>
                <TextField required placeholder='Введите название нового девайса' value={deviceName}/>
                {/* <Button>
                    <Input type='file' />
                </Button> */}
                <Button disabled={deviceName.length === 0} onClick={handleAddingDeviceType}>Добавить</Button>
            </Box>

        </Modal>
    );
}

export default DeviceTypeModal;