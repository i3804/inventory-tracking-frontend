import { Card, CardContent, CardHeader, Typography } from '@mui/material';
import React, { useMemo } from 'react';
import { Tag } from '../../api/models/Tag';
import { useDeviceTypesStore } from '../../store/DeviceTypesStore/provider';
import './styles/index.scss';

interface TagProps {
	data: Tag;
	className?: string;
}

export const TagCard: React.FC<TagProps> = (props): JSX.Element => {
	const {data} = props;
	const deviceTypesStore = useDeviceTypesStore();

	const deviceType = useMemo((): string | undefined => {
		const found = deviceTypesStore.deviceTypes.find(deviceType => deviceType.id === data.device_type);
		if (found) return found.name;
	}, [data.device_type, deviceTypesStore.deviceTypes]);

	const deviceImage = useMemo((): string | undefined => {
		const found = deviceTypesStore.deviceTypes.find(deviceType => deviceType.id === data.device_type);
		if (found) return found.name;
	}, [data.device_type, deviceTypesStore.deviceTypes]);

	return (
		<Card className={[props.className, 'tag-card'].join(' ')}>
			<CardContent>
				<Typography>Метка {data.hash}</Typography>
				<Typography>Image here</Typography>
				{/* <img src={}/> */}
				{data.absence ? <Typography>Была взята: {data?.took_at}</Typography> : <span>На месте</span>}
				<Typography>Тип девайса: {deviceType}</Typography>
			</CardContent>
		</Card>
	);
}

export default TagCard;