import { Card, CardContent, Typography } from '@mui/material';
import React from 'react';
import { DeviceType } from '../../api/models/DeviceType';
import './styles/index.scss';

interface DeviceTypeCardProps {
	data: DeviceType;
}

const DeviceTypeCard: React.FC<DeviceTypeCardProps> = (props): JSX.Element => {
	const {data} = props;

	return (
		<Card className='device-type-card'>
			<CardContent>
				<Typography>img here</Typography>
				<Typography>{data.name}</Typography>
			</CardContent>
		</Card>
	);
}

export default DeviceTypeCard;