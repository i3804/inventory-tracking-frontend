import { ContactlessOutlined, DevicesOtherOutlined } from '@mui/icons-material';
import { Popover, Typography } from '@mui/material';
import React from 'react'
import { useLocation } from 'react-router';
import { NavLink } from 'react-router-dom';
import './styles/index.scss';

const Header: React.FC = (): JSX.Element => {
    const url = useLocation();
    const [anchorElement, setAnchorElement] = React.useState<HTMLElement | null>(null);
    const [activePopover, setActivePopover] = React.useState<'tags' | 'device_types' | null>(null) ;
    
    const handleOpeningPopover = (elem: 'tags' | 'device_types') => (e: React.MouseEvent<HTMLElement>): void => {
        setActivePopover(elem);
        console.log('triggered', e.currentTarget);
        setAnchorElement(e.currentTarget);
    }

    const handleClosingPopover = (): void => {
        setActivePopover(null);
        setAnchorElement(null);
    }

    return (
        <div className='header-wrapper'>
            <NavLink 
                // aria-owns={activePopover === 'tags' ? 'mouse-over-popover' : undefined} 
                // onMouseEnter={handleOpeningPopover('tags')} 
                // onMouseLeave={handleClosingPopover}
                to="/tags" 
                className='header-link'>
                <ContactlessOutlined/>
            </NavLink>
            <NavLink 
                // aria-owns={activePopover === 'device_types' ? 'mouse-over-popover' : undefined} 
                // onMouseEnter={handleOpeningPopover('device_types')} 
                // onMouseLeave={handleClosingPopover} 
                to='/device_types' 
                className='header-link'>
                <DevicesOtherOutlined/>
            </NavLink>
            <Popover 
                id="mouse-over-popover" 
                open={Boolean(anchorElement)} 
                anchorEl={anchorElement}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                  }}
                disableRestoreFocus
                >
                <Typography>{activePopover === 'tags' ? 'Метки' : 'Типы устройств'}</Typography>
            </Popover>
        </div>
    )
}

export default Header;