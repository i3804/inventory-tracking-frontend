export enum SocketEventType {
    DEVICE_ADDED,
    DEVICE_DELETED,
    DEVICE_UPDATED,
    DEVICE_TOGGLED,
    DEVICE_TRACKING_STATE_CHANGED,
    DEVICE_TYPE_ADDED,
    DEVICE_TYPE_DELETED,
}

export interface WebSocketMessageEvent {
    socketEventType: SocketEventType,
    data: Record<string, any>
}