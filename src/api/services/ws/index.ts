import { deviceTypesStore } from '../../../store/DeviceTypesStore/provider';
import { tagStore } from '../../../store/TagStore/provider';
import { DeviceType } from '../../models/DeviceType';
import { Tag } from '../../models/Tag';
import { SocketEventType, WebSocketMessageEvent } from './types';


const SOCKET_URL = `ws://${process.env.REACT_APP_BASE_URL}:8080`;

let wsConnection: WebSocket;

export function establishSocketConnection() {
    if (wsConnection?.OPEN) {
        console.log('no need to open connection again!');
        return;
    }
    wsConnection = new WebSocket(SOCKET_URL);

    wsConnection.onopen = function (this: WebSocket, ev: Event): any {
        console.log('socket connected');
    };

    wsConnection.onclose = function (this: WebSocket, ev: Event): any {
        console.log('socket closed');
    };

    wsConnection.onerror = function (this: WebSocket, ev: Event): any {
        console.log('error with sockets occurred:', ev);
    }
    
    wsConnection.onmessage = function(this: WebSocket, ev: MessageEvent): any {
        try {
            const json = JSON.parse(ev.data.toString()) as WebSocketMessageEvent;
            console.log('message', json);
    
            switch(json.socketEventType) {
                case SocketEventType.DEVICE_ADDED: {
                    tagStore.tags.push(json.data as Tag);
                    break;
                }
                case SocketEventType.DEVICE_DELETED: {
                    tagStore.tags = tagStore.tags.filter((tag) => tag.hash !== json.data.hash);
                    break;
                }
                case SocketEventType.DEVICE_UPDATED:
                case SocketEventType.DEVICE_TOGGLED:
                case SocketEventType.DEVICE_TRACKING_STATE_CHANGED: {
                    const updatedTagIndex = tagStore.tags.findIndex((tag) => tag.hash === json.data.hash);
                    console.log('index', updatedTagIndex);
                    if (updatedTagIndex >= 0) {
                        tagStore.tags[updatedTagIndex] = json.data as Tag;
                    }
                    break;
                }
                case SocketEventType.DEVICE_TYPE_ADDED: {
                    deviceTypesStore.deviceTypes.push(json.data as DeviceType);
                    break;
                }
                case SocketEventType.DEVICE_TYPE_DELETED: {
                    deviceTypesStore.deviceTypes = deviceTypesStore.deviceTypes.filter(deviceType => deviceType.id !== json.data.id);
                    break;
                }
            }
    
        } catch (e) {
            console.log('Error occured!');
        } 
    }
}


export {wsConnection};
