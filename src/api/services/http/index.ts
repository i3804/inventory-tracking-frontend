import axios, { AxiosInstance, AxiosRequestConfig, AxiosRequestHeaders } from "axios";
import { DeviceType } from "../../models/DeviceType";
import { Tag } from "../../models/Tag";


class API {
    private BASE_URL = `http://${process.env.REACT_APP_BASE_URL}:3000`;
    private headers: AxiosRequestHeaders = {
        'Content-Type': 'application/json',
    };
    private defaultConfig: AxiosRequestConfig = {
        baseURL: this.BASE_URL,
        headers: this.headers,
    }

    private request: AxiosInstance = axios.create(this.defaultConfig);

    constructor() {
        // FIX interceptors
        this.request.interceptors.response.use((res) => {
            return res.data;
        }, (err) => {});
    }

    async fetchTags(): Promise<Array<Tag>> {
        return this.request.get('/tag/fetch').then(res => {
            console.log('fetching tags result', res);
            return res as unknown as Array<Tag>;
        }).catch((err) => {
            return Promise.reject(err);
            // do smth
        })
    }

    async changeTagActiveStatus(payload: {hash: string, active: boolean}): Promise<any> {
        return this.request.post(`/tag/${payload.active ? 'activate' : 'deactivate'}`, {hash: payload.hash})
            .then((res) => {
                console.log('result of changing tag status is', res)
            }).catch((err) => {
                return Promise.reject(err);
            })
    }

    async fetchDeviceTypes(): Promise<Array<DeviceType>> {
        return this.request.get('/device_type/fetch').then(res => {
            console.log('fetching device types result', res);
            return res as unknown as Array<DeviceType>;
        }).catch((err) => {
            return Promise.reject(err);
            // do smth
        })
    }

    async createDeviceType(payload: {name: string, picture: Blob}): Promise<any> {
        const formData = new FormData();
        formData.append('name', payload.name);
        formData.append('picture', payload.picture);
        return this.request.post(
            '/device_type/add', 
            formData, 
            {...this.defaultConfig, headers: {'Content-Type': 'application/json'}}
        ).then(res => console.log('result is', res))
        .catch((err) => {
            return Promise.reject(err);
            // do smth
        });
    }

    async deleteDeviceType(id: number): Promise<any> {
        return this.request.post('/device_type/delete', {id}).then(res => {
            console.log('result is', res);
        }).catch((err) => {
            return Promise.reject(err);
            // do smth
        });
    }
}

const api = new API();

export {api};