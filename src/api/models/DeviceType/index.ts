export interface DeviceType {
    id: number;
    name: string;
    picture?: string;
}