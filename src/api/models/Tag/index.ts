export interface Tag {
    id: number;
    device_type: number;
    created_at?: string;
    absence: boolean;
    active: boolean;
    took_at?: string;
    deleted_at?: string;
    hash: string;
}