import React from 'react';
import { BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';
import { api } from './api/services/http';
import { establishSocketConnection } from './api/services/ws';
import Header from './components/Header';
import DeviceTypesPage from './pages/DeviceTypesPage';
import NotFound from './pages/System/NotFound';
import TagPage from './pages/TagPage';
import { DeviceTypesProvider, useDeviceTypesStore } from './store/DeviceTypesStore/provider';
import { TagStoreProvider, useTagStore} from './store/TagStore/provider';

function App() {

  const tagStore = useTagStore();
  const deviceTypesStore = useDeviceTypesStore();

  React.useEffect(() => {
    establishSocketConnection();
    api.fetchTags()
			.then((res) => tagStore.tags = res)
			.catch(err => console.warn('Unable to fetch tags...', err));

    api.fetchDeviceTypes()
      .then((res) => deviceTypesStore.deviceTypes = res)
      .catch(err => console.warn('Unable to fetch device types...', err));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <TagStoreProvider>
      <DeviceTypesProvider>
        <BrowserRouter>
          <Header/>
          <Routes>
            <Route path='/' element={<Navigate to="/tags" replace/>}/>
            <Route path='/tags' element={<TagPage/>}/>
            <Route path='/device_types' element={<DeviceTypesPage/>} />
            <Route path='*' element={<NotFound/>} />
          </Routes>
        </BrowserRouter>
      </DeviceTypesProvider>
    </TagStoreProvider>
  );
}

export default App;
