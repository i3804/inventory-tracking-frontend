import { computed, makeObservable, observable } from "mobx";
import { Tag } from "../../api/models/Tag";


export class TagStore {

    tags: Array<Tag> = [];

    constructor() {
        makeObservable(this, {
            tags: observable,
            presenseTags: computed,
            absenseTags: computed,
            inactiveTags: computed,
        });
    }

    get presenseTags(): Array<Tag> {
        return this.tags.filter(tag => !tag.absence);
    }

    get absenseTags(): Array<Tag> {
        return this.tags.filter(tag => tag.absence);
    }

    get inactiveTags(): Array<Tag> {
        return this.tags.filter(tag => !tag.active);
    }
}

export default TagStore;