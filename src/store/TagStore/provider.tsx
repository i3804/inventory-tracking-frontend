import React from "react";
import TagStore from ".";


export const tagStore = new TagStore();

const TagStoreContext = React.createContext(tagStore);

export const TagStoreProvider = ({children}: any) => <TagStoreContext.Provider value={tagStore}>{children}</TagStoreContext.Provider>

export const useTagStore = () => React.useContext(TagStoreContext);