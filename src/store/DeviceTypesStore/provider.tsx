import React from "react";
import DeviceTypesStore from ".";


export const deviceTypesStore = new DeviceTypesStore();

const DeviceTypesStoreContext = React.createContext(deviceTypesStore);

export const DeviceTypesProvider = ({children}: any) => <DeviceTypesStoreContext.Provider value={deviceTypesStore}>{children}</DeviceTypesStoreContext.Provider>

export const useDeviceTypesStore = () => React.useContext(DeviceTypesStoreContext);