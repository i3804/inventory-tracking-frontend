import { makeObservable, observable } from "mobx";
import { DeviceType } from "../../api/models/DeviceType";


export class DeviceTypesStore {

    deviceTypes: Array<DeviceType> = [];
    
    constructor() {
        makeObservable(this, {
            deviceTypes: observable
        });
    }
}

export default DeviceTypesStore;